#!/usr/bin/python3
#

# X 120-205
# Y 0-120

import serial
import sys
import time
import atexit
import os
import re
import shutil

tif_re = re.compile('.*\.TIF$')

def wait_for_file(timeout):
    old_listing = list(filter(lambda x: tif_re.match(x), os.listdir('/mnt')))
    listing = old_listing
    count = 0

    while len(listing) <= len(old_listing):
        if count >= timeout:
            return False

        time.sleep(1)
        count += 1
        listing = list(filter(lambda x: tif_re.match(x), os.listdir('/mnt')))

    for x in listing:
        if x not in old_listing:
            fname = x

    return fname

def subdivide_interval(minimum, maximum, n_subdivisions):
    index = 0
    minimum = float(minimum)
    maximum = float(maximum)
    delta = (maximum-minimum) / n_subdivisions
    current = minimum

    while index < n_subdivisions:
        yield current
        current += delta
        index += 1

class XRayScanner:
    def __init__(self, serial_port):
        if serial_port == 'dry-run':
            self.port = sys.stdout
        else:
            self.port = serial.Serial(serial_port, 230400, timeout=5.0)

    def home(self):
        self.port.write(b'G28 XY\n')
        self.get_response()

    def goto(self, x,y):
        self.port.write(b'G1 X%d Y%d\n' % (int(x),int(y)))
        self.get_response()

    def xray_on(self):
        print("X-RAY ON!")
        self.port.write(b'M106\n')
        self.get_response()

    def xray_off(self):
        print("X-RAY OFF!")
        self.port.write(b'M107\n')
        self.get_response()

    def delay(self, milliseconds):
        self.port.write(b'G4 P%d\n' % milliseconds)
        self.get_response()

    def wait(self):
        self.port.write(b'M400\n')
        self.get_response()


    def get_response(self, s = b'ok\n'):
        index = 0
        while index < len(s):
            x = self.port.read(1)
            if s[index] == x[0]:
                index += 1
        return True

if __name__ == "__main__":
    scanner = XRayScanner('/dev/ttyACM0')
    begin = (float(sys.argv[1]), float(sys.argv[2]))
    end = (float(sys.argv[3]), float(sys.argv[4]))
    n_subdivisions = int(sys.argv[5])
    count = 0

    scanner.xray_off()

    def turn_off_xray():
        print("turning off xray")
        scanner.xray_off()

    atexit.register(turn_off_xray)

    scanner.home()
    scanner.wait()

    for y in subdivide_interval(begin[1], end[1], n_subdivisions):
        for x in subdivide_interval(begin[0], end[0], n_subdivisions):
            success = False
            while not success:
                print("x=%f, y=%f" % (x,y))
                scanner.goto(x,y)

                # Wait for vibrations
                time.sleep(1)

                scanner.wait()

                scanner.xray_on()
                scanner.delay(950)
                scanner.wait()
                scanner.xray_off()

                filename = wait_for_file(30)
                if not filename:
                    print ("No file! retrying...")
                    continue

                success = True
                count += 1
                new_filename = '/tmp/%d-%f-%f.tif' % (count, x, y)

                print("copying '%s' -> '%s'" % ("/mnt/%s" % filename, new_filename))
                shutil.copy("/mnt/%s" % filename, new_filename)
